		// function for hiding all the result rendering DIVs 
		function doNotLoadStuff(){		
			document.getElementsByClassName('result_render')[0].style.visibility='hidden';
		}
		
		// function for displaying all the result rendering DIVs 
		function doLoadStuff(){		
			document.getElementsByClassName('result_render')[0].style.visibility='visible';		
		}
		
		// function for rolling virtual dice
		function rollVirtualDice(){
			var dice1= Math.floor((Math.random() * 10+2) + 1); //generate random number for dice1
			var dice2= Math.floor((Math.random() * 10) + 1); //generate random number for dice1
			return roll = dice1+dice2;
		}
		// this function works when a player click on "Play" button
		function playLuckySevens(){		
			var start_bet= document.getElementById('txtStartBet').value;
			var roll_cnt1=0; 
			var roll_cnt2=0;
			var tot_roll_cnt=0;
			var max_amt_won=0;
			var max_amt_won_roll_cnt=0;
			
			if(start_bet <= 0 || start_bet== null){
			 document.getElementById("alertmsg1").innerHTML = "<br/><b>Please Enter a Number >0</b>";
			}
			else
			{
				doLoadStuff();
				document.getElementById("st_bet").innerHTML = "<b>$"+ start_bet +"</b>"; // Print Starting Bet Amount in Result Div
				
				// beginning of loop - rolls the dice repeatedly until all the money is gone
				do{
					document.getElementById("alertmsg1").innerHTML = "";
					rollVirtualDice();
				
					// if sum of 2 dice=7 then won $4 else loose $1
					if(roll==7){
							start_bet +=4;
							roll_cnt1++;
						}
					else{
							start_bet -=1;
							roll_cnt2++;
						}
					
					tot_roll_cnt= roll_cnt1+ roll_cnt2;
				
					//comparing maximum won amount with winning roll dice value and start bet amount and store max amount won
					if ( roll==7 && start_bet> max_amt_won)	{
						max_amt_won = Math.round(start_bet * 100) / 100;					
						max_amt_won_roll_cnt=tot_roll_cnt;
					}
				
				} while (start_bet > 0);
				
				// end of loop - rolls the dice repeatedly until all the money is gone
				
				document.getElementById("roll_cnt").innerHTML = "<b>"+ tot_roll_cnt +"</b>"; // Print Total Roll count before going to be completely broke
				document.getElementById("max_amt").innerHTML = "<b>$"+ max_amt_won +"</b>"; // Print Maximum Amount won
				document.getElementById("max_roll_cnt").innerHTML = "<b>"+ max_amt_won_roll_cnt +"</b>"; // Print the count while won the maximum amount
				document.getElementById("playBtn").value= " Play Again "; // Change the button value from "play" to "Play Again"
				document.getElementById('txtStartBet').value =""; // reset starting bet

			}
		}